package by.itstep.mail.sender.controller;

import by.itstep.mail.sender.dto.SendEmailRequestDto;
import by.itstep.mail.sender.service.EmailService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "Controller dedicated to send email message to user")
@Slf4j
@RestController
public class EmailController {

   @Autowired
   private EmailService emailService;

   @PostMapping("/email/send")
   public void send(@RequestBody SendEmailRequestDto request) {
       emailService.sendMimeMessage(request);
   }
}

