package by.itstep.mail.sender.dto;

import by.itstep.mail.sender.dto.enums.ClientServiceName;
import by.itstep.mail.sender.dto.enums.EmailType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class SendEmailRequestDto {

    @ApiModelProperty(notes = "for this ID, get userDto from the account service")
    @NotNull(message = "receiverId can not be null")
    private Integer receiverId;

    @ApiModelProperty(notes = "choose the type of email")
    @NotNull(message = "email type can not be null")
    private EmailType type;

    @ApiModelProperty(example = "Account")
    @NotNull(message = "service name can not be null")
    private ClientServiceName serviceName;

    @ApiModelProperty(example = "text header")
    @NotEmpty(message = "header can not be empty")
    private String header;

    @ApiModelProperty(notes = "text message")
    @NotEmpty(message = "message can not be empty")
    private String message;
}
