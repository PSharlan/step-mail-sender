package by.itstep.mail.sender.dto.enums;

public enum ClientServiceName {

    ACCOUNT,
    TEST,
    ACHIEVEMENT,
    ROADMAP,
    HOMEWORK,
    MENTORING,
    FEEDBACK,
    COWORKING
}
