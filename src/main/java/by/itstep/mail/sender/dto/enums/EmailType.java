package by.itstep.mail.sender.dto.enums;

public enum EmailType {
    NOTIFICATION,
    WARNING,
    PROMO,
    CONGRATULATION
}
