package by.itstep.mail.sender.dto.remote;

import lombok.Data;

import java.time.Instant;

@Data
public class UserDto {

    private String name;
    private String lastName;
    private String email;
    private Double rating;
    private Instant registrationDate;
    private Instant lastLoginDate;
    private String userRole;
}
