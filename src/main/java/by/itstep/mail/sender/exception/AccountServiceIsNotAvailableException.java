package by.itstep.mail.sender.exception;

public class AccountServiceIsNotAvailableException extends RuntimeException{

    public AccountServiceIsNotAvailableException(final String message) {
        super(message);
    }
}
