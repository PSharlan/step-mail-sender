package by.itstep.mail.sender.service;

import by.itstep.mail.sender.dto.SendEmailRequestDto;

public interface EmailService {

    void sendMimeMessage(SendEmailRequestDto requestDto);
}
