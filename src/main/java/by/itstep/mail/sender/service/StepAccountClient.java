package by.itstep.mail.sender.service;

import by.itstep.mail.sender.dto.remote.UserDto;

public interface StepAccountClient {

    UserDto getUserById(Integer userId);
}
