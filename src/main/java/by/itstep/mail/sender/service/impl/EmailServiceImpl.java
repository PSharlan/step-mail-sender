package by.itstep.mail.sender.service.impl;

import by.itstep.mail.sender.dto.SendEmailRequestDto;
import by.itstep.mail.sender.dto.enums.EmailType;
import by.itstep.mail.sender.dto.remote.UserDto;
import by.itstep.mail.sender.service.EmailService;
import by.itstep.mail.sender.service.StepAccountClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;

@Slf4j
@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private StepAccountClient stepAccountClient;

    @Autowired
    private TemplateEngine templateEngine;

    @Value("${app.email.enabled}")
    private boolean sendingEnabled;

    @Override
    @SneakyThrows
    public void sendMimeMessage(SendEmailRequestDto requestDto) {
        if (!sendingEnabled){
            log.warn("Email sending is disabled");
            return;
        }

        UserDto user = stepAccountClient.getUserById(requestDto.getReceiverId());

        Context context = new Context();
        context.setVariable("requestDto", requestDto);
        context.setVariable("user", user);

        String emailTemplate = getEmailTemplate(requestDto.getType());
        String processedHtml = templateEngine.process(emailTemplate, context);

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setTo(user.getEmail());
        helper.setText(processedHtml,true);
        helper.setSubject(requestDto.getHeader());

        mailSender.send(mimeMessage);
        log.info("Message to {} for user: {} {} was sent",user.getEmail(),user.getName(),user.getLastName());
    }

    private String getEmailTemplate(EmailType type) {

        String str = null;

        switch (type) {
            case PROMO: str = "Promo";
            break;
            case WARNING: str = "Warning";
            break;
            case NOTIFICATION: str = "Notification";
            break;
            case CONGRATULATION: str = "Congratulation";
            break;
        }
        return str;
    }
}
