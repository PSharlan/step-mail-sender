package by.itstep.mail.sender.service.impl;

import by.itstep.mail.sender.dto.remote.UserDto;
import by.itstep.mail.sender.exception.AccountServiceIsNotAvailableException;
import by.itstep.mail.sender.service.StepAccountClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class StepAccountClientImpl implements StepAccountClient {

    @Value("${remote.step-account.url}")
    private String stepAccountUrl;

    @Override
    public UserDto getUserById(Integer userId) {

            RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<UserDto> response = restTemplate
                    .getForEntity(stepAccountUrl + "/api/v1/users/" + userId, UserDto.class);
            UserDto body = response.getBody();
            log.info("StepAccountClientImpl -> user found by id: {}", userId);
            return body;
        } catch (Exception e) {
            throw new AccountServiceIsNotAvailableException("Step account is not available!");
        }
    }
}
