package by.itstep.mail.sender;

import by.itstep.mail.sender.dto.SendEmailRequestDto;
import by.itstep.mail.sender.dto.remote.UserDto;
import by.itstep.mail.sender.service.StepAccountClient;
import by.itstep.mail.sender.service.impl.EmailServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static by.itstep.mail.sender.util.DtoGenerationsUtils.generatedSendEmailRequestDto;
import static by.itstep.mail.sender.util.DtoGenerationsUtils.generatedUserDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class ApplicationTests {


	@Autowired
	MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	EmailServiceImpl emailService;

	@MockBean
	StepAccountClient stepAccountClient;

	SendEmailRequestDto requestDto = generatedSendEmailRequestDto();
	UserDto userDto = generatedUserDto();

	@Test
	void sendMimeMessageUsingThymeleafTest_service_happyPath() {
		//given
		Mockito.when(stepAccountClient.getUserById(Mockito.any())).thenReturn(userDto);
		//when
		emailService.sendMimeMessage(requestDto);
	}

	@Test
	void send_controller_happyPath() throws Exception {
		//given
		Mockito.when(stepAccountClient.getUserById(Mockito.any())).thenReturn(userDto);

		//when-then
		mockMvc.perform(post("/email/send")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(requestDto)))
				.andExpect(status().isOk())
				.andReturn();
	}
}
