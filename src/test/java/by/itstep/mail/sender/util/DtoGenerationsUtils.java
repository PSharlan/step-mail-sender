package by.itstep.mail.sender.util;

import by.itstep.mail.sender.dto.SendEmailRequestDto;
import by.itstep.mail.sender.dto.remote.UserDto;

import java.time.Instant;

import static by.itstep.mail.sender.dto.enums.ClientServiceName.TEST;
import static by.itstep.mail.sender.dto.enums.EmailType.CONGRATULATION;

public class DtoGenerationsUtils {

    public static SendEmailRequestDto generatedSendEmailRequestDto() {
        SendEmailRequestDto requestDto = new SendEmailRequestDto();
        requestDto.setHeader("Test message");
        requestDto.setServiceName(TEST);
        requestDto.setType(CONGRATULATION);
        requestDto.setMessage("Test message");
        requestDto.setReceiverId(1111);
        return requestDto;
    }

    public static UserDto generatedUserDto() {
        UserDto userDto = new UserDto();
        userDto.setEmail("dmi.detal@gmail.com");
        userDto.setUserRole("STUDENT");
        userDto.setLastLoginDate(Instant.now());
        userDto.setName("Dmitrij");
        userDto.setLastName("Shlyndikov");
        userDto.setRating(9.0);
        userDto.setRegistrationDate(Instant.now());
        return userDto;
    }

}
